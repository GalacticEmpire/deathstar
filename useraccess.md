# useraccess.md
# This controls the useraccess system and interfaces with the user database
# Provides access to the User database for all employees and contractors
# Used by the Security System to provide proximity access to important systems
# Used by communications systems for email :-p
#
