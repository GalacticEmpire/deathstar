#
# This is the Master Systems Control File
# This controls all Systems and is used on the Bridge
# Requires all systems as prerequisites
# Spits out warning messages for Non responsive systems
# 
# 
#           .          .
# .          .                  .          .              .
#       +.           _____  .        .        + .                    .
#   .        .   ,-~"     "~-.                                +
#              ,^ ___         ^. +                  .    .       .
#             / .^   ^.         \         .      _ .
#            Y  l  o  !          Y  .         __CL\H--.
#    .       l_ `.___.'        _,[           L__/_\H' \\--_-          +
#            |^~"-----------""~ ^|       +    __L_(=): ]-_ _-- -
#  +       . !                   !     .     T__\ /H. //---- -       .
#         .   \                 /               ~^-H--'
#              ^.             .^            .      "       +.
#                "-.._____.,-" .                    .
#         +           .                .   +                       .
#  +          .             +                                  .
#         .             .      .       -Row          Death Star

